#include <strings.h>
#include <stdio.h>

#define boolean int
#define TRUE 1
#define FALSE 0

#define ucase(l) (('a' <= (l) && (l) <= 'z') ? ((l) - 0x20) : (l))

/* dictionary file must be sorted alphabetically, case-insensitive */
#ifndef DICTFILE
#define DICTFILE "/usr/share/dict/words"
#endif

#define NGROUPS 4
#define GROUPSIZE 3

/* #define NO_REUSE */ /* search only for matches that reuse no letters */
/* #define NO_SHORT_GROUPS */ /* require the entire board */
/* #define PRINT_SINGLE_MATCHES */ /* get single words, not full solutions */
#define PRINT_HINTS /* print only "hints" for solutions, not full words */

struct letter {
    char l;
    boolean u;
};

static int match(struct letter[NGROUPS][GROUPSIZE], const char *const,
                 boolean);
static void reset_square(struct letter[NGROUPS][GROUPSIZE],
                         struct letter[NGROUPS][GROUPSIZE]);
static boolean used_all_letters(struct letter[NGROUPS][GROUPSIZE]);
static void set_letter_status(struct letter [NGROUPS][GROUPSIZE], char,
                              boolean);
static void print_word(char *);
static int solve(struct letter[NGROUPS][GROUPSIZE]);

static int
match(
    struct letter square[NGROUPS][GROUPSIZE],
    const char *const word,
    boolean uniq)
{
    int curr_group = -1;
    const char *wordp;

    for (wordp = word; *wordp; ++wordp) {
        char c = ucase(*wordp);
        int i;
        for (i = 0; i < NGROUPS; ++i) {
            int j;
            if (i == curr_group)
                continue;
            for (j = 0; j < GROUPSIZE; ++j) {
                if (uniq && square[i][j].u)
                    continue;
                if (square[i][j].l == c) {
                    square[i][j].u = TRUE;
                    curr_group = i;
                    goto next_letter;
                }
            }
        }
        return 0;
next_letter:
        ;
    }
    return 1;
}

static void
reset_square(
    struct letter orig[NGROUPS][GROUPSIZE],
    struct letter copy[NGROUPS][GROUPSIZE])
{
    int i;
    for (i = 0; i < NGROUPS; ++i) {
        int j;
        for (j = 0; j < GROUPSIZE; ++j) {
            copy[i][j].l = ucase(orig[i][j].l);
            copy[i][j].u = orig[i][j].u;
        }
    }
}

static boolean
used_all_letters(struct letter square[NGROUPS][GROUPSIZE])
{
    int i;
    for (i = 0; i < NGROUPS; ++i) {
        int j;
        for (j = 0; j < GROUPSIZE; ++j) {
            if (!square[i][j].u) {
                return FALSE;
            }
        }
    }
    return TRUE;
}

static void
set_letter_status(
    struct letter square[NGROUPS][GROUPSIZE],
    char target,
    boolean used)
{
    int i;
    target = ucase(target);
    for (i = 0; i < NGROUPS; ++i) {
        int j;
        for (j = 0; j < GROUPSIZE; ++j) {
            if (square[i][j].l == target) {
                square[i][j].u = used;
                return;
            }
        }
    }
}

void
print_word(char *word)
{
#ifdef PRINT_HINTS
    printf("%c", *word);
    printf("%c", *(++word));
    while (*((++word) + 2)) {
        printf("*");
    }
    printf("%c", *word);
    printf("%c", *(++word));
#else
    printf("%s", word);
#endif
}

int
solve(struct letter orig[NGROUPS][GROUPSIZE])
{
#define MAXWORDLEN 50
#define _N2STR(n) #n
#define N2STR(n) _N2STR(n)
    char word[MAXWORDLEN];
    struct letter square[NGROUPS][GROUPSIZE];
    static long offsets[26];
    FILE *fp = fopen(DICTFILE, "r");
#ifndef PRINT_SINGLE_MATCHES
    long fpos;
#endif

    if (!fp) {
        printf("Could not open wordlist file %s", DICTFILE);
        return 1;
    }
    while (fscanf(fp, "%" N2STR(MAXWORDLEN) "[^\n]\n", word) == 1) {
#ifdef NO_REUSE
#define MATCH_UNIQ TRUE
#else
#define MATCH_UNIQ FALSE
#endif
#define get_idx(c) ((c) - (((c) >= 'a') ? 'a' : 'A'))
        reset_square(orig, square);
        /* when we reach a new letter, cache it so we can jump to that part of
         * the dictionary later */
        if (get_idx(*word) && offsets[get_idx(*word)] == 0) {
            offsets[get_idx(*word)] = ftell(fp) - strlen(word) - 1;
        }
        if (match(square, word, MATCH_UNIQ)) {
#ifndef PRINT_SINGLE_MATCHES
            int wordlen = strlen(word);
            char word2[MAXWORDLEN], startchar = word[wordlen - 1];
            struct letter copy[NGROUPS][GROUPSIZE];

            set_letter_status(square, startchar, FALSE);

            fpos = ftell(fp);
            if (get_idx(startchar) == 0 || offsets[get_idx(startchar)]) {
                fseek(fp, offsets[get_idx(startchar)], SEEK_SET);
            }
            while (fscanf(fp, "%" N2STR(MAXWORDLEN) "[^\n]\n", word2) == 1) {
                /* cache new letter position */
                if (get_idx(*word2) && offsets[get_idx(*word2)] == 0) {
                    offsets[get_idx(*word2)] = ftell(fp) - strlen(word2) - 1;
                }
                /* if we go past the intended first letter, stop */
                if (ucase(word2[0]) > ucase(startchar))
                    break;
                /* if are too early, keep going (and record positions) */
                else if (ucase(word2[0]) != ucase(startchar))
                    continue;
                reset_square(square, copy);
                if (match(copy, word2, MATCH_UNIQ)) {
                    if (used_all_letters(copy)) {
                        print_word(word);
                        printf(" ");
                        print_word(word2);
                        printf("\n");
                    }
                }
            }
            fseek(fp, fpos, SEEK_SET);
#else /* PRINT_SINGLE_MATCHES */
            print_word(word);
            printf("\n");
#endif
        }
#undef MATCH_UNIQ
    }

    return 0;

#undef _N2STR
#undef N2STR
#undef MAXWORDLEN
}

int
main(int argc, char **argv)
{
    static const char *failmsg = "Usage: %s ABC DEF GHI JKL\n";
    struct letter orig[NGROUPS][GROUPSIZE];
    int i;

    if ((argc > NGROUPS + 1)
#ifdef NO_SHORT_GROUPS
        || (argc < NGROUPS + 1)
#endif
        ) {
        printf(failmsg, argv[0]);
        return 1;
    }
    
    /* move the arguments into the "letter box" array */
    for (i = 0; i < argc - 1; ++i) {
        int j;
        char *arg = argv[i + 1];
        for (j = 0; j < GROUPSIZE; ++j) {
            if (arg[j] == '\0') {
#if NO_SHORT_GROUPS
                printf(failmsg, argv[0]);
                return 1;
#else
                int k = j;
                while (k < GROUPSIZE) {
                    orig[i][k].l = '\0';
                    orig[i][k].u = TRUE;
                    ++k;
                }
                break;
#endif
            }
            orig[i][j].l = arg[j];
            orig[i][j].u = FALSE;
        }
        if (arg[j] != '\0') {
            printf(failmsg, argv[0]);
            return 1;
        }
    }
    while (i < NGROUPS) {
        int j;
        for (j = 0; j < GROUPSIZE; ++j) {
            orig[i][j].l = '\0';
            orig[i][j].u = TRUE;
        }
        ++i;
    }

    return solve(orig);
}
